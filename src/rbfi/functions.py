import ares
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
from scipy.interpolate import RBFInterpolator
from scipy.interpolate import CubicSpline
import sys
import time

# Names of the relevant directories to store or retrieve data
home = os.getenv("HOME")
models = home + "/oscar-group/data/rbfi/models/"
plots = home + "/oscar-group/data/rbfi/plots/"
delimeter = '-'

def normalize(value,start,stop):
    return (value-start)/(stop-start)


def chi2(observed, expected):
    '''Returns the Chi squared value between 2 lists, treating the first as the observed values and the second as the expected values.'''
    sum = 0
    for i in range(len(observed)):
        sum += ((observed[i] - expected[i]) ** 2) / expected[i]
    return sum


def call_ares (ares_params, redshifts):
    '''
    Returns the temperature fluctuation vs redshift curve.
    INPUTS:
        ares_params: Specify which the values of the desired paramamters.
        redshift: Specify the redshift over which to graph the curve.
    OUTPUTS:
        A cubic spline of the temperature fluctuation vs redshift curve produced by ares.
    '''    
    sim = ares.simulations.Global21cm(**ares_params, verbose=False, progress_bar=False)
    sim.run()
    z = sim.history['z'][::-1]
    dTb = sim.history['dTb'][::-1]
    sorted_idx = np.argsort(z,kind="stable")
    z = z[sorted_idx]
    dTb = dTb[sorted_idx]
    spline = CubicSpline(z, dTb)	
    return spline(redshifts)


def make_rbfi(function, param, N_param, N_redshift, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors):
    '''
    Makes the radial basis function interpolation of a function.
    INPUTS:
        function: The function to be interpolated, signature should be two inputs: parameter and redshift values, and one output: temperature fluctuation.
        param: Theparameter for which to do the interpolation.
        N_redshift: The number of values of the redshift for which to do the interpolation.
        N_param: The number of values of the parameter for which to do the interpolation.
        param_start: The beginning of the plotting domain.
        param_stop: The end of the plotting domain.
        rbfi_kernel: kernel for RBFInterpolator
        rbfi_epsilon: epsilon for RBFInterpolator
        rbfi_neighbors: neighbors for RBFInterpolator
    OUTPUTS:
        None, produces a .pickle file in data/rbfi/models with a name of s hyphen separated values of the arguments used (param, N_redshift, N_param, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors) followed by "rbfi.pickle". Prints the interpolation time and param_domain used.
    '''
    redshift_domain = np.linspace(6, 30, N_redshift)
    param_domain = np.random.rand(N_param) * (param_stop - param_start) + param_start

    d = np.empty([N_redshift * N_param])
    y = np.empty([N_redshift * N_param,2])
    for i in range(N_param):
        d[i * N_redshift:(i + 1) * N_redshift] = function(redshift_domain, param_domain[i])
        y[i * N_redshift:(i + 1) * N_redshift] = np.stack((redshift_domain, np.ones(N_redshift) * param_domain[i]), axis = -1)

    t1 = time.time()
    rbfi = RBFInterpolator(y, d, kernel = rbfi_kernel, epsilon = rbfi_epsilon, neighbors = rbfi_neighbors)
    print("Interpolation Time:", time.time() - t1)

    if (rbfi_neighbors == None):
        rbfi_neighbors = "all"

    file_name = models + param + delimeter + str(N_param) + delimeter + str(N_redshift) + delimeter + str(param_start) + delimeter + str(param_stop)+ delimeter + rbfi_kernel + delimeter + str(rbfi_epsilon) + delimeter + str(rbfi_neighbors) + delimeter + "rbfi.pickle"
    with open(file_name, "wb") as f:
        pickle.dump(rbfi,f)
    print(param_domain)


def plot_compare(model_name, function, param_val, show_plot = True,start = 0,stop = 1 ):
    '''
    Plots the model of a function versus the actual function.
    INPUTS:
        model_name: The name of the model file, as it appears in data/rbfi/models.
        function: The function that was interpolated.
        param_value: The value of the paramter to be used.
        show_plot: Whether to display the plot immediately.
    Outputs:
        None, saves the image and plt object of the plot as hyphen separated values of the arguments used (param, N_redshift, N_param, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors) followed by "rbfi-compare" in data/rbfi/plots. Prints the maximum difference, average difference, and Chi squared between the two curves.
    '''
    # Parsing the the model name as the file name and graphing values
    graphing_values = model_name.split(delimeter)
    param = graphing_values[0]
    N_redshift = int(graphing_values[1])

    file_name = models + model_name
    redshift_domain = np.linspace(6, 30, N_redshift)

    # Load the model
    with open(file_name, "rb") as f:
        rbfi = pickle.load(f)

    func_curve = function(redshift_domain, param_val, param)


    model_curve = rbfi(np.stack((redshift_domain, normalize(param_val,start,stop) * np.ones(N_redshift)), axis = -1))

    # Plotting functions on the same graph and saving them to data/rbfi/plots
    plt.plot(redshift_domain, func_curve, label = "Original Curve")
    plt.plot(redshift_domain, model_curve, label = "Modelled Curve")
    plt.legend()
    plt.xlabel("redshift")
    plt.ylabel("Temperature fluctuation (" + param + " is varied)")

    file_signature = plots + model_name[:-7] + delimeter + str(param_val) + delimeter + "compare"

    plt.savefig(file_signature + ".png")
    plot_name = file_signature + ".pickle"
    with open(plot_name, "wb") as f:
        pickle.dump(plt.gcf(), f)
    if (show_plot == True):
        plt.show()
    plt.close()


    # Printing error metrics
    ave_diff = np.mean(np.abs(func_curve - model_curve))
    max_diff = np.max(np.abs(func_curve - model_curve))
    with open(file_signature + '.txt', 'w') as f:
        f.write("The average difference is:" + str(ave_diff))
        f.write('\n' + "The maximum difference is:" +str(max_diff))
        f.write('\n' + "The Chi Sqaured is:" + str(chi2(func_curve, model_curve)))
    print("The average difference is:", ave_diff)
    print("The maximum difference is:", max_diff)
    print("The Chi Squared is:", chi2(func_curve, model_curve))


def plot_param_space(model_name, show_plot = True):
    '''
    Plots the model of a function in 3D space as a function of redshift and parameter value.
    INPUTS:
        model_name: The name of the model file, as it appears in data/rbfi/models.
        show_plot: Whether to display the plot immediately.
    Outputs:
        None, saves the image and plt object of the plot as hyphen separated values of the arguments used (param, N_redshift, N_param, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors) followed by "rbfi-3D" in data/rbfi/plots.
    '''
    # Parsing the the model name as the file name and graphing values
    graphing_values = model_name.split(delimeter)
    param = graphing_values[0]
    N_redshift = int(graphing_values[1])
    N_param = int(graphing_values[2])
    param_start = float(graphing_values[3])
    param_stop = float(graphing_values[4])
    param_domain = np.linspace(param_start, param_stop, N_param)

    file_name = models + model_name
    redshift_domain = np.linspace(6, 30, N_redshift)

    # Load the model
    with open(file_name, "rb") as f:
        rbfi = pickle.load(f)

    
    # Plot the model
    ax = plt.axes(projection='3d')
    for param_val in param_domain:
        a_domain = param_val * np.ones(N_redshift)
        ax.plot3D(redshift_domain, a_domain, rbfi(np.stack((redshift_domain, param_val * np.ones(N_redshift)), axis = -1)))
    
    ax.set_xlabel("Redshift")
    ax.set_ylabel(param)
    ax.set_zlabel("dTb")

    file_signature = plots + model_name[:-7] + delimeter + "3D"

    plt.savefig(file_signature + ".png")
    plot_name = file_signature + ".pickle"
    with open(plot_name, "wb") as f:
        pickle.dump(ax, f)
    plt.close()

def view_plot(file_name):
    '''
    Display a plot from data/rbfi/plots.
    INPUTS:
        file_name: name of the .pickle file to be graphed
    OUTPUTS:
        None, displays the plot
    '''
    plot_type = file_name.split(delimeter)[-1].split('.')[0]
    file_name = plots + file_name
    with open(file_name, "rb") as f:
        figure = pickle.load(f)
    if (show_plot == True):
        plt.show()
    plt.close()
