import functions as fn
import numpy as np
import scipy as sp
import pickle
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression


# Loading the interpolation and testing points
gem_path = fn.home + "/oscar-group/data/21cmGEM/"
train_file = gem_path + "train_dup.p"
test_file = gem_path + "test_dup.p"

with open(train_file, "rb") as f:
    train = pickle.load(f)
with open(test_file, "rb") as f:
    test = pickle.load(f)


# Separating the parameters from the temperatures
y_train = train.T[:-1].T
d_train = train.T[-1].T
y_test = test.T[:-1].T
d_test = test.T[-1].T

# Normalizing
y_train = normalize(y_train, axis=0, norm='max')
# d_train = d_train / np.abs(d_train).max()
y_test = normalize(y_test, axis=0, norm='max')
# d_test = d_test / np.abs(d_test).max()

amount = 0.0625#0.09305
N = int(amount * d_test.shape[0])
N = 38 * 451
train = True

if train == True:
    rbfi = sp.interpolate.RBFInterpolator(y_train[:N], d_train[:N])
    with open(fn.models + "21cmFAST_RBFI_" + str(int(N / 451)) + ".p", "wb") as f:
        pickle.dump(rbfi, f)
else:
    with open(fn.models + "21cmFAST_RBFI_10.p", "rb") as f:
        rbfi = pickle.load(f)

n = 2
y = y_train[n * 451:(n + 1) * 451]
d = d_train[n * 451:(n + 1) * 451]

plt.scatter(y.T[-1], d)
plt.plot(y.T[-1], rbfi(y))
plt.show()
