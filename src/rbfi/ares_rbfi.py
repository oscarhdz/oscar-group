import argparse
import functions as fn


def main():
    # DEFAULT ARGS:
    param = "fX"
    N_redshift = 100
    N_param = 100
    param_start = 0
    param_stop = 1
    rbfi_kernel = "gaussian"
    rbfi_epsilon = 1
    rbfi_neighbors = None

    # Checks for user specified arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--param")
    parser.add_argument("-n", "--N_redshift", type = int)
    parser.add_argument("-N", "--N_param", type = int)
    parser.add_argument("-s", "--param_start", type = float)
    parser.add_argument("-S", "--param_stop", type = float)

    parser.add_argument("-k", "--rbfi_kernel")
    parser.add_argument("-e", "--rbfi_epsilon", type = float)
    parser.add_argument("-E", "--rbfi_neighbors", type = int)
    args = parser.parse_args()

    if (args.param != None):
        param = args.param
    if (args.N_redshift != None):
        N_redshift = args.N_redshift
    if (args.N_param != None):
        N_param = args.N_param
    if (args.param_start != None):
        param_start = args.param_start
    if (args.param_stop != None):
        param_stop = args.param_stop
    
    if (args.rbfi_kernel != None):
        rbfi_kernel = args.rbfi_kernel
    if (args.rbfi_epsilon != None):
        rbfi_epsilon = args.rbfi_epsilon
    if (args.rbfi_neighbors != None):
        rbfi_neighbors = args.rbfi_neighbors


    # Function that returns the dTb/redshift curve from ares
    def func(x, a, param = "fX"):
        dict = {param: a}
        curve = fn.call_ares(dict, x)
        return curve

    # Makes the rbfi model
    fn.make_rbfi(func, param, N_param, N_redshift, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors)
    

if __name__ == "__main__":
    main()
