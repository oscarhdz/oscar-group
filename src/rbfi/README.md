# RBFI
## Summary

This section contains tools and scripts pertaining to the use of radial basis function interpolation ([RBFI](https://en.wikipedia.org/wiki/Radial_basis_function_interpolation)).

## List of Files and Functions:
### [ares_rbfi.py](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/src/rbfi/ares_rbfi.py) produces an RBFU model of the ARES simulation.

A .pickle file containing the model is placed in [data/rbfi/models](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/data/rbfi/models). This file is used from the command line with optional arguments:

    python ares_rbfi.py -p [parameter] -n [parameter-amount] -N [redshifts-amount] -s [start] -S [stop] -k [rbfi_kernel] -e [rbfi_epsilon] -E [rbfi_neighbors]


### [plot_compare.py](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/src/rbfi/plot_compare.py) graphs a function versus a RBFI model and prints error metrics.

The plot is saved to [data/rbfi/plots](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/data/rbfi/plots). Maximum difference, average difference, and Chi squared are reported. This file is used from the command line with optional arguments:

    python plot_compare.py -m [model-file-name] -v [parameter-value] -s [show_plot] -t [start] -T [stop]

### [plot_3D.py](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/src/rbfi/plot_3D.py) plots a model over its parameter and redshift.
The plot is saved to [data/rbfi/plots](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/data/rbfi/plots). This file is used from the command line with optional arguments:

    python plot_compare.py -m [model-file-name] -s [show_plot]

### [functions.py](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/src/rbfi/functions.py) contains many of the useful functions used by the scripts in the directory:

- chi2 returns the Chi squared value between 2 lists, treating the first as the observed values and the second as the expected values.

- call_ares returns the temperature fluctuation vs redshift curve.
    - INPUTS:
       - ares_params: Specify which the values of the desired paramamters.
       - redshift: Specify the redshift over which to graph the curve.
    - OUTPUTS:
        - A cubic spline of the temperature fluctuation vs redshift curve produced by ares.

- make_rbfi makes the radial basis function interpolation of a function.
    - INPUTS:
        - function: The function to be interpolated, signature should be two inputs: parameter and redshift values, and one output: temperature fluctuation.
        - param: The parameter for which to do the interpolation.
        - N_redshift: The number of values of the redshift for which to do the interpolation.
        - N_param: The number of values of the parameter for which to do the interpolation.
        - param_start: The beginning of the plotting domain.
        - param_stop: The end of the plotting domain.
        - rbfi_kernel: kernel for RBFInterpolator
        - rbfi_epsilon: epsilon for RBFInterpolator
        - rbfi_neighbors: neighbors for RBFInterpolator
    - OUTPUTS:
        - None, produces a .pickle file in [data/rbfi/models](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/data/rbfi/models) with a name of s hyphen separated values of the arguments used (param, N_redshift, N_param, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors) followed by "rbfi.pickle". Prints the interpolation time and param_domain used.

- plot_compare plots the model of a function versus the actual function.
    - INPUTS:
        - model_name: The name of the model file, as it appears in [data/rbfi/models](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/data/rbfi/models).
        - function: The function that was interpolated.
        - param_value: The value of the paramter to be used.
        - show_plot: Whether to display the plot immediately.
    - Outputs:
        - None, saves the image and plt object of the plot as hyphen separated values of the arguments used (param, N_redshift, N_param, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors, param_val) followed by "rbfi-compare" in data/rbfi/plots. Prints the maximum difference, average difference, and Chi squared between the two curves.

- plot_param_space plots the model of a function in 3D space as a function of redshift and parameter value.
    - INPUTS:
        - model_name: The name of the model file, as it appears in data/rbfi/models.
        - show_plot: Whether to display the plot immediately.
    - Outputs:
        - None, saves the image and plt object of the plot as hyphen separated values of the arguments used (param, N_redshift, N_param, param_start, param_stop, rbfi_kernel, rbfi_epsilon, rbfi_neighbors) followed by "rbfi-3D" in data/rbfi/plots.

- view_plot displays a plot from data/rbfi/plots.
    - INPUTS:
        - file_name: name of the .pickle file to be graphed
    - OUTPUTS:
        - None, displays the plot
