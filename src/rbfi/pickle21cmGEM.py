import functions as fn
import numpy as np
import pickle


# 21cmFAST model file locations
gem_path = fn.home + "/oscar-group/data/21cmGEM/"
train_par_path = gem_path + "Par_train_21cmGEM.txt"
train_temp_path = gem_path + "T21_train_21cmGEM.txt"
test_par_path = gem_path + "Par_test_21cmGEM.txt"
test_temp_path = gem_path + "T21_test_21cmGEM.txt"
train_par = np.loadtxt(train_par_path)
train_temp = np.loadtxt(train_temp_path)
test_par = np.loadtxt(test_par_path)
test_temp = np.loadtxt(test_temp_path)

# The redshift domain
z_N = 451
z_start = 5
z_stop = 50
z_domain = np.linspace(z_start, z_stop, z_N)
train_N = train_par.shape[0]
test_N = test_par.shape[0]
par_N = train_par.shape[1]

z_domain_train_tile = np.array([np.tile(z_domain, train_N)])
z_domain_test_tile = np.array([np.tile(z_domain, test_N)])

# The sets are the original parameter combinations tiled for each redshift value, the redshift and temperatures are then appended at the end.
y_train = np.tile(train_par.T, z_N)
y_train = np.concatenate((y_train, z_domain_train_tile), axis = 0)
d_train = np.array([np.concatenate(train_temp, axis = 0)])
train = np.concatenate((y_train, d_train), axis = 0).T

y_test = np.tile(test_par.T, z_N)
y_test = np.concatenate((y_test, z_domain_test_tile), axis = 0)
d_test = np.array([np.concatenate(test_temp, axis = 0)])
test = np.concatenate((y_test, d_test), axis = 0).T

# Remove duplicates, somehow they exist
#train = np.unique(train, axis = 0)
#test = np.unique(test, axis = 0)

train_file = gem_path + "train_dup.p"
test_file = gem_path + "test_dup.p"
with open(train_file, "wb") as f:
    pickle.dump(train, f)
with open(test_file, "wb") as f:
    pickle.dump(test, f)
