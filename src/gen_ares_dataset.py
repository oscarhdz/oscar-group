import numpy as np
import pymp
import ares
from call_ares import call_ares
import sys
sys.path.append("/home/s/sievers/hans1/oscar-group/src/nis")
from ares_params import ares_params, redshifts, denormalize

N = 10000 #number of curves to compute
num_params = len(ares_params)

curves_array = pymp.shared.array([N,redshifts.shape[0]])
params_array = pymp.shared.array([N,len(ares_params)])

#randomly choosing output values
params_array = denormalize(np.random.rand(N,len(ares_params)))

with pymp.Parallel(40) as p:
	for index in p.range(0,N):
		input_dict = {}
		for i in range(num_params):
			input_dict[ares_params[i][0]] = params_array[index][i]
		curves_array[index] = call_ares(input_dict, redshifts)

np.savetxt("/scratch/s/sievers/hans1/ares_curve_dataset.gz", curves_array)
np.savetxt("/scratch/s/sievers/hans1/ares_params_dataset.gz", params_array)
