import numpy as np
import pymp
import ares
#import os
#os.chdir("/home/s/sievers/hans1/ares/scripts")
from call_ares import call_ares
#os.chdir("/home/s/sievers/hans1/NEATnik/nis")
from ares_params import ares_params, redshifts, denormalize

N = 1000 #number of training points without the added corner points
num_params = len(ares_params)

inputs_array = pymp.shared.array([N + 2**num_params,redshifts.shape[0]])

outputs_array = pymp.shared.array([N+2**num_params,len(ares_params)])

#randomly choosing output values
outputs_array[0:N] = np.random.rand(N,len(ares_params)) #normalized until later in the script

num_params = len(ares_params)

#adding all the corners
def gcv (n):
    p = np.zeros([2**n,n])
    for i in range(2**n):
        num = i
        for j in range(n):
            if num >= 2**(n-j-1):
                num -= 2**(n-j-1)
                p[i][j] = 1
    return p

outputs_array[-2**num_params:] = gcv(num_params)
outputs_array = denormalize(outputs_array) #denormalizing outputs array

with pymp.Parallel(40) as p:
	for index in p.range(0,N+num_params**2):
		input_dict = {}
		for i in range(num_params): #for some reason, this line breaks if you replace num_params with len(ares_params)
			input_dict[ares_params[i][0]] = outputs_array[index][i]
		inputs_array[index] = call_ares(input_dict, redshifts)

np.savetxt("/project/s/sievers/hans1/nis/ares_training_inputs.gz", inputs_array)
np.savetxt("/project/s/sievers/hans1/nis/ares_training_outputs.gz", outputs_array)

#generating five test curves
test_params = denormalize(np.random.rand(5,len(ares_params)))

test_curves = np.empty([5,redshifts.shape[0]])
for i in range(5):
	input_dict = {}
	for j in range(num_params):
		input_dict[ares_params[j][0]] = test_params[i][j]
	test_curves[i] = call_ares(input_dict, redshifts)

np.savetxt("/project/s/sievers/hans1/nis/ares_test_params.gz", test_params)
np.savetxt("/project/s/sievers/hans1/nis/ares_test_curves.gz", test_curves)
