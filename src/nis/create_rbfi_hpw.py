import numpy as np
from curve_details import curve_details
from ares_params import redshifts, normalize
from scipy.interpolate import RBFInterpolator
import os
import pickle

if __name__ == "__main__":
	curves = np.loadtxt("/project/s/sievers/hans1/nis/ares_training_inputs.gz")
	cd = curve_details(curves)
	np.savetxt("ares_training_curve_details.gz",cd)

	Param = normalize(np.loadtxt("/project/s/sievers/hans1/nis/ares_training_outputs.gz"))
	
	rbfilist = []
	for i in range(cd.shape[1]):
		rbfilist.append(RBFInterpolator(Param, cd[:,i], kernel = "gaussian", epsilon = 1))

	#saving the rbf interpolators
	os.chdir("/project/s/sievers/hans1/nis")
	if not "hpw_rbfis" in os.listdir():
		os.mkdir("hpw_rbfis")
	os.chdir("hpw_rbfis")
	for i in range(len(rbfilist)):
		with open("rbfi"+str(i)+".pickle", "wb") as f:
			pickle.dump(rbfilist[i], f)
