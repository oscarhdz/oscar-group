import numpy as np
from scipy.interpolate import RBFInterpolator
from ares_params import redshifts

def call_rbfi(rbfi, params):
	return rbfi(np.append(redshifts[np.newaxis].T, np.tile(params,(redshifts.shape[0],1)), axis = 1))

def call_rbfi_many(rbfi, params):
	# the same function but expecting a list of params
	# returns an array of curve values
	nrs = redshifts.shape[0] #stands for num redshifts
	nc = params.shape[0] #stands for num curves
	
	rs_stacked = np.tile(redshifts[np.newaxis].T, (nc,1))
	params_repeated = np.repeat(params, nrs, axis = 0)

	curves_approxed = rbfi(np.hstack((rs_stacked, params_repeated))) #this has dimensions [nrs*nc,1]
	return curves_approxed.reshape((nc,nrs))
