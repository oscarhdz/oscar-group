import numpy as np
import py21cmfast as p21c

N = 160 #number of training points
H_eff_min = 10
H_eff_max = 100
LX_min = 20
LX_max = 40

def run_sim(HII_eff,L_X):
	n = 25
	redshifts = list(np.linspace(5,20,n))
	coevals = p21c.run_coeval(
	redshift = redshifts,
	user_params = {"HII_DIM": 100, "BOX_LEN": 100, "N_THREADS": 40, "USE_INTERPOLATION_TABLES": True},
	astro_params = p21c.AstroParams({"HII_EFF_FACTOR":HII_eff, "L_X":L_X}),
	random_seed=12345)

	global_Tb = np.empty(n)
	for j in range(n):
		global_Tb[j] = coevals[j].brightness_temp_struct.global_Tb
	return global_Tb

inputs_array = np.empty([N,25])
outputs_array = np.empty([N,2])
#randomly choosing output values
outputs_array[:,0] = np.random.rand(N)*(H_eff_max-H_eff_min)+H_eff_min
outputs_array[:,1] = np.random.rand(N)*(LX_max-LX_min)+LX_min

for i in range(N):
	if i%4 == 0: print(str(i*100.0/N)+"%")
	inputs_array[i] = run_sim(outputs_array[i][0], outputs_array[i][1])

np.savetxt("/project/s/sievers/hans1/abc/training_inputs.gz", inputs_array)
np.savetxt("/project/s/sievers/hans1/abc/training_outputs.gz", outputs_array)

#generating five test curves
test_params = np.empty([5,2])
test_params[:,0] = np.random.rand(5)*(H_eff_max-H_eff_min)+H_eff_min
test_params[:,1] = np.random.rand(5)*(LX_max-LX_min)+LX_min

test_curves = np.empty([5,25])
for i in range(5):
	test_curves[i] = run_sim(test_params[i][0], test_params[i][1])

np.savetxt("/project/s/sievers/hans1/abc/test_params.gz", test_params)
np.savetxt("/project/s/sievers/hans1/abc/test_curves.gz", test_curves)

#from __future__ import print_function
