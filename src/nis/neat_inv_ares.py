#import
import neatnik
import parameters

# Typing:
from neatnik import Experiment
from neatnik import Organism

# Others:
import numpy as np
import pickle
from ares_params import ares_params, redshifts, normalize, denormalize
from call_rbfi import call_rbfi_many
#import pymp #for parallelizing the fitness function
import time

#loading the rbfi
with open("/project/s/sievers/hans1/nis/ares_rbfi.pickle", "rb") as file:
    rbfi = pickle.load(file)

display_score = np.empty(neatnik.Parameters.generational_cycles+1)

class inv_ares(Experiment):
    """ Find parameters from an ARES sim """

    def __init__(self) -> None:
        """ Initializes experiment """

        super().__init__()
	
        number_of_inputs = redshifts.shape[0]

        vertexes_list = []
        for i in range(number_of_inputs):
            vertexes_list.append((i, None, neatnik.ENABLED, neatnik.INPUT,  neatnik.IDENTITY, 0, i))
        
        vertexes_list.append((number_of_inputs,     None, neatnik.ENABLED, neatnik.BIAS,   neatnik.UNITY,    0, 0))
        
        for i in range(len(ares_params)): #adding all the parameters as outputs
            vertexes_list.append((number_of_inputs + 1 + i, None, neatnik.ENABLED, neatnik.OUTPUT, neatnik.LOGISTIC, 1, 10*i))

        self.vertexes = vertexes_list

        edges_list = []
        for i in range(len(ares_params)): #connecting the biasing node to the output nopdes
            edges_list.append((None, None, neatnik.ENABLED, neatnik.BIASING, number_of_inputs, number_of_inputs + 1 + i, None))
        self.edges = edges_list
        
        #loading from pre-generated files
        training_inputs = np.loadtxt("/project/s/sievers/hans1/nis/ares_training_inputs.gz")
        training_outputs = normalize(np.loadtxt("/project/s/sievers/hans1/nis/ares_training_outputs.gz"))

        #using these as the stimulation and responses
        self.stimuli = np.array([training_inputs])
        self.responses = np.array([training_outputs])

        self.gc = 0 #stands for generation count

    def fitness(self, organism: Organism) -> float:
        """ Scores the fitness of the input Organism. """
        reactions = organism.react()
	
        
 #       interp_curves = pymp.shared.array([self.responses.shape[1],redshifts.shape[0]])
 #       with pymp.Parallel(40) as p:
 #           for index in p.range(0,self.responses.shape[1]):
 #               interp_curves[index] = call_rbfi(rbfi, reactions[0][index])

        interp_curves = call_rbfi_many(rbfi, np.asarray(reactions[0]))

        differences = np.abs(interp_curves-self.stimuli[0])
        
        #exponential function
        #return np.sum(np.exp(-differences))/(differences.shape[0]*differences.shape[1])

        #rational function
        return 1.0/(1.0+np.mean(differences))

    def display(self) -> None:
        """ Displays information about the Experiment on the screen. """
        max_score = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].score 
        
        print("Max. Fitness:", "%.5f"%max_score, end="\r", flush=True)
        #print("Max fitness", max_score, flush = True)

        display_score[self.gc] = max_score
        self.gc += 1
	
        return

t1 = time.time()
experiment = inv_ares()
experiment.run()
t2 = time.time()
print("Total time difference:", t2-t1)

if experiment.MPI_rank == 0:

    #input("\nNEATnik has finished.")

    # print(experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].graph())

    max_score = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].score                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            #print("Max. Fitness:", "%.2f"%max_score, end="\r", flush=True)                                                                                                                                                                                                                                                              print("Max fitness", max_score, flush = True)
    print("Max fitness", max_score)
    
    np.savetxt("/project/s/sievers/hans1/nis/display_score.gz", display_score)

    organism = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0];
    pickle.dump(organism.graph(), open('/project/s/sievers/hans1/nis/organism.p', 'wb'))
