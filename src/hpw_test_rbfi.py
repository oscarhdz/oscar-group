import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
import os
import time
from pathlib import Path
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/nis") #this should be wherever call_rbfi and ares_params are saved
from call_rbfi_hpw import call_rbfi_many
from ares_params import redshifts, normalize
from curve_details import curve_details

p = Path(__file__).parents[1]
testcd = curve_details(np.loadtxt(str( p)+ "/data/example_data/July 9th/ares_July_9_5_test_curves.gz"))#change this to wherever your list of curves is stored
testParams = np.loadtxt(str( p)+ "/data/example_data/July 9th/ares_July_9_test_params.gz") #change this to wherever your list of parameters is stored
normalizedTestparams = normalize(testParams)

os.chdir(str( p)+ "/data/rbfi/models/hpw_rbfis") #go to wherever your rbfis are stored
rbfilist = []

for i in range(len(os.listdir())):
    with open("rbfi"+str(i)+".pickle", "rb") as file:
        rbfilist.append(pickle.load(file))

if __name__ == "__main__":
	t1 = time.time()
	rbficd = call_rbfi_many(rbfilist, normalizedTestparams)
	print("Average time to get curve from interpolator:", (time.time()-t1)/5)

	print("True curve detilas:")
	print(testcd)
	print("Approximated curve details:")
	print(rbficd)
	print("Maximum height difference:", np.max(np.abs(testcd[:,0]-rbficd[:,0])))
	print("Average height difference:", np.mean(np.abs(testcd[:,0]-rbficd[:,0])))
	print("Maximum position difference:", np.max(np.abs(testcd[:,1]-rbficd[:,1])))
	print("Average position difference:", np.mean(np.abs(testcd[:,1]-rbficd[:,1])))
	print("Maximum width difference:", np.max(np.abs(testcd[:,2]-rbficd[:,2])))
	print("Average width difference:", np.mean(np.abs(testcd[:,2]-rbficd[:,2])))
