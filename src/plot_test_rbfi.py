import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
import time
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/nis") #this should be wherever call_rbfi and ares_params are saved
from call_rbfi import call_rbfi
from ares_params import redshifts, normalize

testCurves = np.loadtxt("curves.gz") #change this to wherever your list of curves is stored
testParams = np.loadtxt("params.gz") #change this to wherever your list of parameters is stored
normalizedTestparams = normalize(testParams)

with open("ares_rbfi.pickle", "rb") as file: #change this to where your rbfi pickle is located
	rbfi = pickle.load(file)


if __name__ == "__main__":
	plt.plot(redshifts, testCurves[0], color="r", ls=":", label="true curve")
	plt.plot(redshifts, testCurves[1], color="g", ls=":")
	plt.plot(redshifts, testCurves[2], color="b", ls=":")
	plt.plot(redshifts, testCurves[3], color="c", ls=":")
	plt.plot(redshifts, testCurves[4], color="m", ls=":")
	
	rbfiCurves = np.empty(testCurves.shape)
	rbfiCurves[0] = call_rbfi(rbfi, normalizedTestparams[0])
	t1 = time.time()
	rbfiCurves[1] = call_rbfi(rbfi, normalizedTestparams[1])
	t3 = time.time(); rbfiCurves[2] = call_rbfi(rbfi, normalizedTestparams[2]); print("Time for one:", time.time()-t3)
	rbfiCurves[3] = call_rbfi(rbfi, normalizedTestparams[3])
	rbfiCurves[4] = call_rbfi(rbfi, normalizedTestparams[4])
	print("Average time to get curve from interpolator:", (time.time()-t1)/4)

	plt.plot(redshifts, rbfiCurves[0], color="r", ls="-", label = "rbfi approx.")
	plt.plot(redshifts, rbfiCurves[1], color="g", ls="-")
	plt.plot(redshifts, rbfiCurves[2], color="b", ls="-")
	plt.plot(redshifts, rbfiCurves[3], color="c", ls="-")
	plt.plot(redshifts, rbfiCurves[4], color="m", ls="-")
	plt.xlabel(r"$z$")
	plt.ylabel(r"$\delta T_b$ (mK)")
	plt.title("Comparing test curves to their rbfi approximations")
	plt.legend()
	plt.savefig("outputs/rbfi_testplot")

	print("Maximum difference:", np.max(np.abs(testCurves-rbfiCurves)))
	print("Average difference:", np.mean(np.abs(testCurves-rbfiCurves)))
